#!/bin/bash

SALT_BUILD_VERS="${SALT_BRANCH:-v3005}"

pip install virtualenv

pip install -r saltpkgconf/requirements.txt

pkgr --log-level=debug -c saltpkgconf/redhat_legacy/redhat_legacy.conf --git=https://github.com/saltstack/salt.git --ref=$SALT_BUILD_VERS

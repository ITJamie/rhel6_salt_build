%global __brp_check_rpaths %{nil}

%bcond_with tests
%bcond_with docs

# Disable build-id symlinks
%define _build_id_links none
%undefine _missing_build_ids_terminate_build

# Disable private libraries from showing in provides
%global __provides_exclude_from ^.*\\.so.*$
%global __requires_exclude_from ^.*\\.so.*$

%define fish_dir %{_datadir}/fish/vendor_functions.d
%define _pkg_version 1


Name:    salt
Version: %{_version}
Release: %{_pkg_version}%{?dist}
Summary: A parallel remote execution system
Group:   System Environment/Daemons
License: ASL 2.0
URL:     http://saltstack.org/
Source3: requirements.txt
Source10: README.fedora
Source11: %{name}-common.logrotate
Source12: salt.bash
Source22: salt
Source24: salt-minion
Source32: salt-call
Source33: salt-proxy
Source35: salt-pip
Source36: rcd-salt-minion


BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: x86_64

%ifarch %{ix86} x86_64
Requires: dmidecode
%endif

Requires: pciutils
Requires: which



#BuildRequires: python3
#BuildRequires: python3-devel
#BuildRequires: openssl-devel
BuildRequires: git

%description
Salt is a distributed remote execution system used to execute commands and
query data. It was developed in order to bring the best solutions found in
the world of remote execution together and make them better, faster and more
malleable. Salt accomplishes this via its ability to handle larger loads of
information, and not just dozens, but hundreds or even thousands of individual
servers, handle them quickly and through a simple and manageable interface.



%package    minion
Summary:    Client component for Salt, a parallel remote execution system
Group:      System Environment/Daemons
Requires:   %{name} = %{version}-%{release}

%description minion
The Salt minion is the agent component of Salt. It listens for instructions
from the master, runs jobs, and returns results back to the master.

%build
ls -lah $CI_PROJECT_DIR/
tiamat_tar=$CI_PROJECT_DIR/artifacts/*/salt*.tar.gz
mv_dstdir=/tmp/onedir_output
rm -rf $mv_dstdir
mkdir $mv_dstdir
tar xvf $tiamat_tar -C $mv_dstdir

mkdir -p %{buildroot}/opt/saltstack/salt
# pip installs directory
mkdir -p %{buildroot}/opt/saltstack/salt/pypath/
cp -ra $mv_dstdir/salt %{buildroot}/opt/saltstack/

sources_loc=$CI_PROJECT_DIR/saltpkgconf/redhat_legacy/sources/*
cp -r $sources_loc %{_sourcedir}

# Add some directories
install -d -m 0755 %{buildroot}%{_var}/log/salt
touch %{buildroot}%{_var}/log/salt/minion
install -d -m 0755 %{buildroot}%{_var}/cache/salt
install -d -m 0755 %{buildroot}%{_sysconfdir}/salt
install -d -m 0755 %{buildroot}%{_sysconfdir}/salt/minion.d
install -d -m 0755 %{buildroot}%{_sysconfdir}/salt/pki
install -d -m 0700 %{buildroot}%{_sysconfdir}/salt/pki/minion
install -d -m 0755 %{buildroot}%{_sysconfdir}/salt/proxy.d
install -d -m 0755 %{buildroot}%{_bindir}

# Add helper scripts
install -m 0755 %{SOURCE22} %{buildroot}%{_bindir}/salt
install -m 0755 %{SOURCE24} %{buildroot}%{_bindir}/salt-minion
install -m 0755 %{SOURCE32} %{buildroot}%{_bindir}/salt-call
install -m 0755 %{SOURCE33} %{buildroot}%{_bindir}/salt-proxy
install -m 0755 %{SOURCE35} %{buildroot}%{_bindir}/salt-pip

# Add the config files
install -p -m 0640 %{_sourcedir}/config_files/minion %{buildroot}%{_sysconfdir}/salt/minion

# Add the initd file
mkdir -p %{buildroot}%{_initrddir}
install -p -m 0755 %{SOURCE36} %{buildroot}%{_initrddir}/salt-minion

# Logrotate
install -p %{SOURCE10} .
mkdir -p %{buildroot}%{_sysconfdir}/logrotate.d/
install -p -m 0644 %{SOURCE11} %{buildroot}%{_sysconfdir}/logrotate.d/salt

# Bash completion
mkdir -p %{buildroot}%{_sysconfdir}/bash_completion.d/
install -p -m 0644 %{SOURCE12} %{buildroot}%{_sysconfdir}/bash_completion.d/salt.bash


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%{_sysconfdir}/logrotate.d/salt
%{_sysconfdir}/bash_completion.d/salt.bash
%{_var}/cache/salt
%{_var}/log/salt
/opt/saltstack/
/opt/saltstack/salt
/opt/saltstack/salt/run
/opt/saltstack/salt/pypath

%{_bindir}/salt-pip
%{_bindir}/salt
%config(noreplace) %{_sysconfdir}/salt/
#%config(noreplace) %{_sysconfdir}/salt/pki

%files minion
%defattr(-,root,root)
%{_bindir}/salt-minion
%{_bindir}/salt-call
%{_bindir}/salt-proxy
%config(noreplace) %{_sysconfdir}/salt/minion
%config(noreplace) %{_sysconfdir}/salt/minion.d
%config(noreplace) %{_sysconfdir}/salt/pki/minion
%attr(0755, root, root) %{_initrddir}/salt-minion


%post minion
  /sbin/chkconfig --add salt-minion


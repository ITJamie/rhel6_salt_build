# About
This build of salt is community built for rhel6. It uses tiamat (onedir).

# Unsupported
This is unsupported, i nor saltstack(vmware) will support this.

If you are using this you REALLY need to migrate away from rhel6, this exists only to help folks with managing legacy setups while migrating.

# 3005.1 is PROBABLY THE LAST EVER BUILD FOR RHEL6
v3005.1 is likely the last possible release for rhel6 as salt have begun removing rhel6 specific code from the saltcodebase.



Download pre-built versions here:
https://gitlab.com/ITJamie/rhel6_salt_build/-/packages



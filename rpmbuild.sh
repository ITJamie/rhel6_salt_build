#!/bin/bash
set -x
cd $CI_PROJECT_DIR/saltpkgconf/redhat_legacy/
rpmbuild --define "_version $SALT_BUILD_NAME" -ba salt.spec 
mkdir -p $CI_PROJECT_DIR/artifacts/rpms
cp /root/rpmbuild/RPMS/x86_64/*.rpm $CI_PROJECT_DIR/artifacts/rpms/
